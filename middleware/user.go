package middleware

import (
	"net/http"
	"strings"
	"yoga-api/models"

	"github.com/gin-gonic/gin"
)

func Auth(us models.UserService) gin.HandlerFunc {
	return func(c *gin.Context) {
		header := c.GetHeader("Authorization")
		if len(strings.TrimSpace(header)) <= 7 {
			c.Abort()
			c.JSON(http.StatusUnauthorized, gin.H{
				"message": "unauthorized",
			})
			return
		}
		token := strings.TrimSpace(header[len("Bearer "):])
		if token == "" {
			c.Abort()
			c.JSON(http.StatusUnauthorized, gin.H{
				"message": "unauthorized",
			})
			return
		}

		user, err := us.GetUserByToken(token)
		if err != nil {
			c.Abort()
			c.JSON(http.StatusUnauthorized, gin.H{
				"message": "unauthorized",
			})
			return
		}

		c.Set("user", user)
	}
}
