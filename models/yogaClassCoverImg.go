package models

import (
	"log"
	"mime/multipart"
	"os"
	"path/filepath"
	"yoga-api/rand"

	"github.com/jinzhu/gorm"
)

const YogaClassCoverImgServerUrl = "https://yoga-api.twilightparadox.com/yogaClassCoverImg/"

type YogaClassCoverImgTable struct {
	gorm.Model
	Filename string `gorm:"NOT NULL"`
	Url      string `gorm:"NOT NULL"`
}
type YogaClassCoverImgRes struct {
	Filename string `json:"filename"`
	Url      string `json:"url"`
}

func (YogaClassCoverImgTable) Tablename() string {
	return "yogaClassCoverImages"
}

type YogaClassCoverImgService interface {
	CreateYogaClassCoverImg(file *multipart.FileHeader) (*YogaClassCoverImgTable, error)
	DeleteYogaClassCoverImgById(id uint) error
	GetYogaClassCoverImgById(id uint) (*YogaClassCoverImgTable, error)
}

var _ YogaClassCoverImgService = &YogaClassCoverImgGorm{}

type YogaClassCoverImgGorm struct {
	db *gorm.DB
}

func NewYogaClassCoverImgService(db *gorm.DB) YogaClassCoverImgService {
	return &YogaClassCoverImgGorm{db}
}

func (YogaClassCoverImgGorm *YogaClassCoverImgGorm) CreateYogaClassCoverImg(file *multipart.FileHeader) (*YogaClassCoverImgTable, error) {
	dir := filepath.Join(UploadPath, "yogaCoverImages")
	if err := os.MkdirAll(dir, os.ModePerm); err != nil {
		log.Printf("create gallery dir error: %v\n", err)
		return nil, err
	}
	tx := YogaClassCoverImgGorm.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while uploading photo")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return nil, err
	}
	generated, err := rand.GetToken()
	if err != nil {
		log.Printf("error generating filename: %v\n", err)
		tx.Rollback()
		return nil, err
	}
	ext := filepath.Ext(file.Filename)
	filename := generated[:len(generated)-1] + ext
	image := YogaClassCoverImgTable{
		Filename: filename,
		Url:      YogaClassCoverImgServerUrl + filename,
	}
	if err := tx.Create(&image).Error; err != nil {
		log.Printf("create image error: %v\n", err)
		tx.Rollback()
		return nil, err
	}
	if err := saveFile(file, image.FilePath()); err != nil {
		tx.Rollback()
		return nil, err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return nil, err
	}
	return &image, nil
}

func (img *YogaClassCoverImgTable) FilePath() string {
	return filepath.Join(UploadPath, "yogaCoverImages", img.Filename)
}

func (YogaClassCoverImgGorm *YogaClassCoverImgGorm) GetYogaClassCoverImgById(id uint) (*YogaClassCoverImgTable, error) {
	image := new(YogaClassCoverImgTable)
	if err := YogaClassCoverImgGorm.db.Where("deleted_at IS NULL AND id = ?", id).First(&image).Error; err != nil {
		return nil, err
	}
	return image, nil
}

func (YogaClassCoverImgGorm *YogaClassCoverImgGorm) DeleteYogaClassCoverImgById(id uint) error {
	image, err := YogaClassCoverImgGorm.GetYogaClassCoverImgById(id)
	if err != nil {
		return err
	}
	tx := YogaClassCoverImgGorm.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while deleting image")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return err
	}
	if err := tx.Where("id = ?", id).Unscoped().Delete(&YogaClassCoverImgTable{}).Error; err != nil {
		log.Printf("Fail deleting image in DB: %v\n", err)
		tx.Rollback()
		return err
	}
	err = os.Remove(image.FilePath())
	if err != nil {
		log.Printf("Fail deleting image: %v\n", err)
		tx.Rollback()
		return err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return err
	}
	return nil
}
