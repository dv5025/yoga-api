package models

import (
	"log"
	"mime/multipart"
	"os"
	"path/filepath"
	"yoga-api/rand"

	"github.com/jinzhu/gorm"
)

const PostImgServerUrl = "https://yoga-api.twilightparadox.com/postImg/"

type PostImgTable struct {
	gorm.Model
	Filename string `gorm:"NOT NULL"`
	Url      string `gorm:"NOT NULL"`
}

type PostImgRes struct {
	Filename string `json:"filename"`
	Url      string `json:"url"`
}

func (PostImgTable) TableName() string {
	return "postImg"
}

var _ PostImgService = &PostImgGorm{}

type PostImgGorm struct {
	db *gorm.DB
}

type PostImgService interface {
	AddPostImage(file *multipart.FileHeader) (*PostImgTable, error)
	GetPostImgResById(id uint) (*PostImgRes, error)
	DeletePostImageById(id uint) error
}

func NewPostImgService(db *gorm.DB) PostImgService {
	return &PostImgGorm{db}
}

func (piG *PostImgGorm) AddPostImage(file *multipart.FileHeader) (*PostImgTable, error) {
	dir := filepath.Join(UploadPath, "postImg")
	if err := os.MkdirAll(dir, os.ModePerm); err != nil {
		log.Printf("create post image dir error: %v\n", err)
		return nil, err
	}
	tx := piG.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while uploading post image")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return nil, err
	}
	generated, err := rand.GetToken()
	if err != nil {
		log.Printf("error generating filename: %v\n", err)
		tx.Rollback()
		return nil, err
	}
	ext := filepath.Ext(file.Filename)
	filename := generated[:len(generated)-1] + ext
	image := PostImgTable{
		Filename: filename,
		Url:      PostImgServerUrl + filename,
	}
	if err := tx.Create(&image).Error; err != nil {
		log.Printf("create post image error: %v\n", err)
		tx.Rollback()
		return nil, err
	}
	if err := saveFile(file, image.FilePath()); err != nil {
		tx.Rollback()
		return nil, err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return nil, err
	}
	return &image, nil
}

func (img *PostImgTable) FilePath() string {
	return filepath.Join(UploadPath, "postImg", img.Filename)
}

func (piG *PostImgGorm) GetPostImgResById(id uint) (*PostImgRes, error) {
	image := new(PostImgTable)
	err := piG.db.Where("id = ?", id).First(image).Error
	res := new(PostImgRes)
	if err == nil {
		res.Filename = image.Filename
		res.Url = image.Url
	} else if err.Error() == "record not found" {
		res.Filename = ""
		res.Url = ""
	} else if err != nil {
		return nil, err
	}
	return res, nil
}

func (piG *PostImgGorm) DeletePostImageById(id uint) error {
	tx := piG.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while deleting post image")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return err
	}
	image := new(PostImgTable)
	if err := tx.Where("id = ?", id).First(image).Error; err != nil {
		return err
	}
	if err := tx.Where("id = ?", id).Unscoped().Delete(&PostImgTable{}).Error; err != nil {
		log.Printf("Fail deleting image in DB: %v\n", err)
		tx.Rollback()
		return err
	}
	err := os.Remove(image.FilePath())
	if err != nil {
		log.Printf("Fail deleting image: %v\n", err)
		tx.Rollback()
		return err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return err
	}
	return nil
}
