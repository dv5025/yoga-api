package models

import (
	"log"

	"github.com/jinzhu/gorm"
)

type PoseDescriptionTable struct {
	gorm.Model
	PoseID      uint
	Step        uint
	Description string
}

func (PoseDescriptionTable) TableName() string {
	return "posesDescription"
}

type PoseDesRes struct {
	Step        uint   `json:"step"`
	Description string `json:"description"`
}

type CreatePoseDesReq struct {
	PoseID      uint
	Step        uint
	Description string
}

var _ PoseDescriptionService = &PoseDescriptionGorm{}

type PoseDescriptionGorm struct {
	db *gorm.DB
}

type PoseDescriptionService interface {
	CreatePoseDescription(data *CreatePoseDesReq) (*PoseDescriptionTable, error)
	GetPoseDesByPoseId(poseID uint) (*[]PoseDescriptionTable, error)
	GetPoseDesResByPoseId(poseID uint) (*[]PoseDesRes, error)
}

func NewPoseDescriptionService(db *gorm.DB) PoseDescriptionService {
	return &PoseDescriptionGorm{db}
}

func (poseDescriptionGorm *PoseDescriptionGorm) CreatePoseDescription(data *CreatePoseDesReq) (*PoseDescriptionTable, error) {
	tx := poseDescriptionGorm.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while creating pose description")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return nil, err
	}
	poseDes := new(PoseDescriptionTable)
	poseDes.PoseID = data.PoseID
	poseDes.Step = data.Step
	poseDes.Description = data.Description
	if err := tx.Create(poseDes).Error; err != nil {
		log.Printf("create pose description error: %v\n", err)
		tx.Rollback()
		return nil, err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return nil, err
	}
	return poseDes, nil
}

func (pDesG *PoseDescriptionGorm) GetPoseDesByPoseId(poseID uint) (*[]PoseDescriptionTable, error) {
	descriptions := new([]PoseDescriptionTable)
	if err := pDesG.db.Where("deleted_at IS NULL AND pose_id = ?", poseID).Order("step asc").Find(descriptions).Error; err != nil {
		return nil, err
	}
	return descriptions, nil
}

func (pDesG *PoseDescriptionGorm) GetPoseDesResByPoseId(poseID uint) (*[]PoseDesRes, error) {
	descriptions := new([]PoseDescriptionTable)
	if err := pDesG.db.Where("deleted_at IS NULL AND pose_id = ?", poseID).Order("step asc").Find(descriptions).Error; err != nil {
		return nil, err
	}
	res := []PoseDesRes{}
	for _, des := range *descriptions {
		temp := new(PoseDesRes)
		temp.Step = des.Step
		temp.Description = des.Description
		res = append(res, *temp)
	}
	return &res, nil
}

func GetPoseDesResByPoseId(poseID uint, db *gorm.DB) (*[]PoseDesRes, error) {
	descriptions := new([]PoseDescriptionTable)
	if err := db.Where("deleted_at IS NULL AND pose_id = ?", poseID).Order("step asc").Find(descriptions).Error; err != nil {
		return nil, err
	}
	res := []PoseDesRes{}
	for _, des := range *descriptions {
		temp := new(PoseDesRes)
		temp.Step = des.Step
		temp.Description = des.Description
		res = append(res, *temp)
	}
	return &res, nil
}
