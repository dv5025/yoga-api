package models

import (
	"log"

	"github.com/jinzhu/gorm"
)

type DefinedPoseTable struct {
	gorm.Model
	YogaClassID      uint `gorm:"NOT NULL"`
	YogaClassLevelID uint
	PoseID           uint `gorm:"NOT NULL"`
	Time             uint `gorm:"NOT NULL"`
	Step             uint `gorm:"NOT NULL"`
}

func (DefinedPoseTable) TableName() string {
	return "definedPoses"
}

type CreateDefinedPoseReq struct {
	PoseID uint
	Time   uint
}

type DefinedPoseRes struct {
	ID               uint    `json:"id"`
	YogaClassID      uint    `json:"yogaClassId"`
	YogaClassLevelID uint    `json:"yogaClassLevelId"`
	Poses            PoseRes `json:"poses"`
	Time             uint    `json:"time"`
	Step             uint    `json:"step"`
}

var _ DefinedPoseService = &DefinedPoseGorm{}

type DefinedPoseGorm struct {
	db *gorm.DB
}

type DefinedPoseService interface {
	AddDefinedPose(classID uint, levelID uint, poseID uint, time uint, step uint) error
	GetDefinedPoseResByLevelId(levelID uint) (*[]DefinedPoseRes, error)
	GetDefinedPoseResByClassId(classID uint) (*[]DefinedPoseRes, error)
	GetDefinedPoseResByClassIdAndLevelId(classID uint, levelID uint) (*[]DefinedPoseRes, error)
	GetDefinedPoseByLevelId(levelID uint) (*[]DefinedPoseTable, error)
	GetDefinedPoseByClassId(classID uint) (*[]DefinedPoseTable, error)
	GetDefinedPoseByClassIdAndLevelId(classID uint, levelID uint) (*[]DefinedPoseTable, error)
}

func NewDefinedPoseService(db *gorm.DB) DefinedPoseService {
	return &DefinedPoseGorm{db}
}

func (dpG *DefinedPoseGorm) AddDefinedPose(classID uint, levelID uint, poseID uint, time uint, step uint) error {
	definedPose := new(DefinedPoseTable)
	definedPose.YogaClassID = classID
	definedPose.YogaClassLevelID = levelID
	definedPose.PoseID = poseID
	definedPose.Time = time
	definedPose.Step = step
	tx := dpG.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while adding history")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return err
	}
	if err := tx.Create(definedPose).Error; err != nil {
		log.Printf("create history error: %v\n", err)
		tx.Rollback()
		return err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return err
	}
	return nil
}

func (dpG *DefinedPoseGorm) GetDefinedPoseResByLevelId(levelID uint) (*[]DefinedPoseRes, error) {
	definedPoseTable, err := dpG.GetDefinedPoseByLevelId(levelID)
	if err != nil {
		return nil, err
	}
	res := []DefinedPoseRes{}
	for _, definedPose := range *definedPoseTable {
		pose, err := GetPoseResById(definedPose.PoseID, dpG.db)
		if err != nil {
			return nil, err
		}
		temp := new(DefinedPoseRes)
		temp.ID = definedPose.ID
		temp.YogaClassID = definedPose.YogaClassID
		temp.YogaClassLevelID = definedPose.YogaClassID
		temp.Poses = *pose
		temp.Time = definedPose.Time
		temp.Step = definedPose.Step
		res = append(res, *temp)
	}
	return &res, nil
}

func (dpG *DefinedPoseGorm) GetDefinedPoseResByClassId(classID uint) (*[]DefinedPoseRes, error) {
	definedPoseTable, err := dpG.GetDefinedPoseByClassId(classID)
	if err != nil {
		return nil, err
	}
	res := []DefinedPoseRes{}
	for _, definedPose := range *definedPoseTable {
		pose, err := GetPoseResById(definedPose.PoseID, dpG.db)
		if err != nil {
			return nil, err
		}
		temp := new(DefinedPoseRes)
		temp.ID = definedPose.ID
		temp.YogaClassID = definedPose.YogaClassID
		temp.YogaClassLevelID = definedPose.YogaClassID
		temp.Poses = *pose
		temp.Time = definedPose.Time
		temp.Step = definedPose.Step
		res = append(res, *temp)
	}
	return &res, nil
}

func (dpG *DefinedPoseGorm) GetDefinedPoseResByClassIdAndLevelId(classID uint, levelID uint) (*[]DefinedPoseRes, error) {
	definedPoseTable, err := dpG.GetDefinedPoseByClassIdAndLevelId(classID, levelID)
	if err != nil {
		return nil, err
	}
	res := []DefinedPoseRes{}
	for _, definedPose := range *definedPoseTable {
		pose, err := GetPoseResById(definedPose.PoseID, dpG.db)
		if err != nil {
			return nil, err
		}
		temp := new(DefinedPoseRes)
		temp.ID = definedPose.ID
		temp.YogaClassID = definedPose.YogaClassID
		temp.YogaClassLevelID = definedPose.YogaClassID
		temp.Poses = *pose
		temp.Time = definedPose.Time
		temp.Step = definedPose.Step
		res = append(res, *temp)
	}
	return &res, nil
}

func (dpG *DefinedPoseGorm) GetDefinedPoseByLevelId(levelID uint) (*[]DefinedPoseTable, error) {
	res := new([]DefinedPoseTable)
	if err := dpG.db.Where("yoga_class_level_id = ?", levelID).Find(res).Error; err != nil {
		return nil, err
	}
	return res, nil
}

func (dpG *DefinedPoseGorm) GetDefinedPoseByClassId(classID uint) (*[]DefinedPoseTable, error) {
	res := new([]DefinedPoseTable)
	if err := dpG.db.Where("yoga_class_id = ?", classID).Find(res).Error; err != nil {
		return nil, err
	}
	return res, nil
}

func (dpG *DefinedPoseGorm) GetDefinedPoseByClassIdAndLevelId(classID uint, levelID uint) (*[]DefinedPoseTable, error) {
	res := new([]DefinedPoseTable)
	if err := dpG.db.Where("yoga_class_id = ? AND yoga_class_level_id = ?", classID, levelID).Find(res).Error; err != nil {
		return nil, err
	}
	return res, nil
}
