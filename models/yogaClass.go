package models

import (
	"log"
	"time"

	"github.com/jinzhu/gorm"
)

type YogaClassTable struct {
	gorm.Model
	Name         string `gorm:"NOT NULL" default:"null" json:"name"`
	Descriotion  string `gorm:"NOT NULL" default:"null" json:"descriotion"`
	IsPublic     bool   `gorm:"NOT NULL" json:"isPublic"`
	UserID       uint   `gorm:"NOT NULL" json:"userID"`
	CoverImageID uint   `gorm:"NOT NULL" json:"coverImageID"`
	Type         string `gorm:"NOT NULL" json:"type"`
}

func (YogaClassTable) Tablename() string {
	return "yogaClasses"
}

type YogaClassRes struct {
	ID          uint                     `json:"id"`
	Name        string                   `json:"name"`
	Image       YogaClassCoverImgRes     `json:"image"`
	Description string                   `json:"description"`
	Type        string                   `json:"type"`
	Levels      []YogaClassLevelShortRes `json:"levels"`
	UpdatedAt   time.Time                `json:"updated_at"`
	CreatedAt   time.Time                `json:"created_at"`
}

type CreateCustomClassReq struct {
	Name        string
	Description string
	Poses       []CreateDefinedPoseReq
}

type CustomClassRes struct {
	ID          uint                 `json:"id"`
	Name        string               `json:"name"`
	Image       YogaClassCoverImgRes `json:"image"`
	Description string               `json:"description"`
	Type        string               `json:"type"`
	UserID      uint                 `json:"userId"`
	Poses       []DefinedPoseRes     `json:"poses"`
	UpdatedAt   time.Time            `json:"updated_at"`
	CreatedAt   time.Time            `json:"created_at"`
}

type YogaClassShortRes struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
}

type CreateYogaClassReq struct {
	Name         string
	Descriotion  string
	IsPublic     bool
	UserID       uint
	CoverImageID uint
}

type YogaClassService interface {
	CreateYogoClass(data *CreateYogaClassReq) (*YogaClassTable, error)
	CreateCustomClass(name string, description string, userID uint) (*YogaClassTable, error)
	GetAllPresetYogaClass() (*[]YogaClassTable, error)
	GetPresetYogaClassById(id uint) (*YogaClassTable, error)
	GetCustomClassByUserId(id uint) (*[]YogaClassTable, error)
	GetCustomClassById(id uint) (*YogaClassTable, error)
	GetYogaClassShortResById(id uint) (*YogaClassShortRes, error)
	GetYogaClassById(id uint) (*YogaClassTable, error)
}

var _ YogaClassService = &YogaClassGorm{}

type YogaClassGorm struct {
	db *gorm.DB
}

func NewYogaClassService(db *gorm.DB) YogaClassService {
	return &YogaClassGorm{db}
}

func (yogaClassGorm *YogaClassGorm) CreateCustomClass(name string, description string, userID uint) (*YogaClassTable, error) {
	yogaClass := new(YogaClassTable)
	yogaClass.Name = name
	yogaClass.Descriotion = description
	yogaClass.IsPublic = false
	yogaClass.UserID = userID
	yogaClass.CoverImageID = 0
	yogaClass.Type = "CUSTOM"
	tx := yogaClassGorm.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while creating gallery")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return nil, err
	}
	if err := tx.Create(yogaClass).Error; err != nil {
		log.Printf("create gallery error: %v\n", err)
		tx.Rollback()
		return nil, err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return nil, err
	}
	return yogaClass, nil
}

func (yogaClassGorm *YogaClassGorm) CreateYogoClass(data *CreateYogaClassReq) (*YogaClassTable, error) {
	yogaClass := new(YogaClassTable)
	yogaClass.Name = data.Name
	yogaClass.Descriotion = data.Descriotion
	yogaClass.IsPublic = data.IsPublic
	yogaClass.UserID = data.UserID
	yogaClass.CoverImageID = data.CoverImageID
	tx := yogaClassGorm.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while creating gallery")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return nil, err
	}
	if err := tx.Create(yogaClass).Error; err != nil {
		log.Printf("create gallery error: %v\n", err)
		tx.Rollback()
		return nil, err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return nil, err
	}
	return yogaClass, nil
}

func (ycG *YogaClassGorm) GetAllPresetYogaClass() (*[]YogaClassTable, error) {
	yogaClasses := new([]YogaClassTable)
	if err := ycG.db.Where("deleted_at IS NULL AND type = ?", "PRESET").Find(yogaClasses).Error; err != nil {
		return nil, err
	}
	return yogaClasses, nil
}

func (ycG *YogaClassGorm) GetPresetYogaClassById(id uint) (*YogaClassTable, error) {
	yogaClasse := new(YogaClassTable)
	if err := ycG.db.Where("deleted_at IS NULL AND type = ? AND id = ?", "PRESET", id).First(yogaClasse).Error; err != nil {
		return nil, err
	}
	return yogaClasse, nil
}

func (ycG *YogaClassGorm) GetYogaClassShortResById(id uint) (*YogaClassShortRes, error) {
	yogaClasse := new(YogaClassTable)
	if err := ycG.db.Where("deleted_at IS NULL AND id = ?", id).First(yogaClasse).Error; err != nil {
		return nil, err
	}
	res := new(YogaClassShortRes)
	res.ID = yogaClasse.ID
	res.Name = yogaClasse.Name
	return res, nil
}

func (ycG *YogaClassGorm) GetCustomClassByUserId(id uint) (*[]YogaClassTable, error) {
	yogaClasse := new([]YogaClassTable)
	if err := ycG.db.Where("deleted_at IS NULL AND type = ? AND user_id = ?", "CUSTOM", id).Find(yogaClasse).Error; err != nil {
		return nil, err
	}
	return yogaClasse, nil
}

func (ycG *YogaClassGorm) GetCustomClassById(id uint) (*YogaClassTable, error) {
	yogaClasse := new(YogaClassTable)
	if err := ycG.db.Where("deleted_at IS NULL AND type = ? AND id = ?", "CUSTOM", id).First(yogaClasse).Error; err != nil {
		return nil, err
	}
	return yogaClasse, nil
}

func (ycG *YogaClassGorm) GetYogaClassById(id uint) (*YogaClassTable, error) {
	yogaClasse := new(YogaClassTable)
	if err := ycG.db.Where("deleted_at IS NULL AND id = ?", id).First(yogaClasse).Error; err != nil {
		return nil, err
	}
	return yogaClasse, nil
}
