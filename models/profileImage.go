package models

import (
	"log"
	"mime/multipart"
	"os"
	"path/filepath"
	"yoga-api/rand"

	"github.com/jinzhu/gorm"
)

const ProfileImgServerUrl = "https://yoga-api.twilightparadox.com/user/profileImage/"

type ProfileImageTable struct {
	gorm.Model
	UserID   uint   `gorm:"NOT NULL"`
	Filename string `gorm:"NOT NULL"`
	Url      string `gorm:"NOT NULL"`
}

type ProfileImageRes struct {
	Filename string `json:"filename"`
	Url      string `json:"url"`
}

func (ProfileImageTable) TableName() string {
	return "profileImages"
}

var _ ProfileImageService = &ProfileImageGorm{}

type ProfileImageGorm struct {
	db *gorm.DB
}

type ProfileImageService interface {
	EditProfileImage(files *multipart.FileHeader, userID uint) (*ProfileImageTable, error)
	GetProfileImageByUserId(userID uint) (*ProfileImageTable, error)
}

func NewProfileImageService(db *gorm.DB) ProfileImageService {
	return &ProfileImageGorm{db}
}

func (proImgG *ProfileImageGorm) EditProfileImage(file *multipart.FileHeader, userID uint) (*ProfileImageTable, error) {
	dir := filepath.Join(UploadPath, "profileImg")
	if err := os.MkdirAll(dir, os.ModePerm); err != nil {
		log.Printf("create gallery dir error: %v\n", err)
		return nil, err
	}
	tx := proImgG.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while uploading photo")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return nil, err
	}

	profileImage, err := proImgG.GetProfileImageByUserId(userID)
	if profileImage != nil {
		// if user has profile image
		if err := tx.Where("user_id = ?", userID).Unscoped().Delete(&ProfileImageTable{}).Error; err != nil {
			log.Printf("Fail deleting image in DB: %v\n", err)
			tx.Rollback()
			return nil, err
		}
		err = os.Remove(profileImage.FilePath())
		if err != nil {
			log.Printf("Fail deleting image: %v\n", err)
			tx.Rollback()
			return nil, err
		}
		generated, err := rand.GetToken()
		if err != nil {
			log.Printf("error generating filename: %v\n", err)
			tx.Rollback()
			return nil, err
		}
		ext := filepath.Ext(file.Filename)
		filename := generated[:len(generated)-1] + ext
		newProfileImage := new(ProfileImageTable)
		newProfileImage.UserID = userID
		newProfileImage.Filename = filename
		newProfileImage.Url = ProfileImgServerUrl + filename
		if err := tx.Create(&newProfileImage).Error; err != nil {
			log.Printf("create profile image error: %v\n", err)
			tx.Rollback()
			return nil, err
		}
		if err := saveFile(file, newProfileImage.FilePath()); err != nil {
			tx.Rollback()
			return nil, err
		}
		if err := tx.Commit().Error; err != nil {
			log.Printf("transaction commit: %v\n", err)
			return nil, err
		}
		return newProfileImage, nil
	} else if err.Error() == "record not found" {
		// if user dose not have profile image
		generated, err := rand.GetToken()
		if err != nil {
			log.Printf("error generating filename: %v\n", err)
			tx.Rollback()
			return nil, err
		}
		ext := filepath.Ext(file.Filename)
		filename := generated[:len(generated)-1] + ext
		newProfileImage := new(ProfileImageTable)
		newProfileImage.UserID = userID
		newProfileImage.Filename = filename
		newProfileImage.Url = ProfileImgServerUrl + filename
		if err := tx.Create(&newProfileImage).Error; err != nil {
			log.Printf("create profile image error: %v\n", err)
			tx.Rollback()
			return nil, err
		}
		if err := saveFile(file, newProfileImage.FilePath()); err != nil {
			tx.Rollback()
			return nil, err
		}
		if err := tx.Commit().Error; err != nil {
			log.Printf("transaction commit: %v\n", err)
			return nil, err
		}
		return newProfileImage, nil
	} else {
		return nil, err
	}
}

func (img *ProfileImageTable) FilePath() string {
	return filepath.Join(UploadPath, "profileImg", img.Filename)
}

func (proImgG *ProfileImageGorm) GetProfileImageByUserId(userID uint) (*ProfileImageTable, error) {
	profileImage := new(ProfileImageTable)
	if err := proImgG.db.Where("user_id = ?", userID).First(profileImage).Error; err != nil {
		return nil, err
	}
	return profileImage, nil
}
