package models

import (
	"log"
	"time"

	"github.com/jinzhu/gorm"
)

type HistoryTable struct {
	gorm.Model
	UserID           uint `gorm:"NOT NULL"`
	YogaClassID      uint
	YogaClassLevelID uint
}

type AddHistoryReq struct {
	ClassID uint
	LevelID uint
}

type HistoryRes struct {
	ID        uint              `json:"id"`
	UserID    uint              `json:"userId"`
	YogaClass YogaClassShortRes `json:"yogaClass"`
	LevelName string            `json:"levelName"`
	Poses     []DefinedPoseRes  `json:"poses"`
	CreateAt  time.Time         `json:"created_at"`
}

func (HistoryTable) TableName() string {
	return "histories"
}

var _ HistoryService = &HistoryGorm{}

type HistoryGorm struct {
	db *gorm.DB
}

type HistoryService interface {
	AddHistory(userId uint, classID uint, levelID uint) error
	GetHistoriesByUserId(userID uint) (*[]HistoryTable, error)
}

func NewHistoryService(db *gorm.DB) HistoryService {
	return &HistoryGorm{db}
}

func (hg *HistoryGorm) AddHistory(userId uint, classID uint, levelID uint) error {
	history := new(HistoryTable)
	history.UserID = userId
	history.YogaClassID = classID
	history.YogaClassLevelID = levelID
	tx := hg.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while adding history")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return err
	}
	if err := tx.Create(history).Error; err != nil {
		log.Printf("create history error: %v\n", err)
		tx.Rollback()
		return err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return err
	}
	return nil
}

func (hg *HistoryGorm) GetHistoriesByUserId(userID uint) (*[]HistoryTable, error) {
	histories := new([]HistoryTable)
	if err := hg.db.Where("deleted_at IS NULL AND user_id = ?", userID).Order("created_at desc").Find(histories).Error; err != nil {
		return nil, err
	}
	return histories, nil
}
