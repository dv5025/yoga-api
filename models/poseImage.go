package models

import (
	"log"
	"mime/multipart"
	"os"
	"path/filepath"
	"yoga-api/rand"

	"github.com/jinzhu/gorm"
)

const PoseImageServerUrl = "https://yoga-api.twilightparadox.com/poses/images/"

type PoseImageTable struct {
	gorm.Model
	Filename string `gorm:"NOT NULL"`
	Url      string `gorm:"NOT NULL"`
}

func (PoseImageTable) TableName() string {
	return "posesImages"
}

type PoseImageRes struct {
	Filename string `json:"filename"`
	Url      string `json:"url"`
}

var _ PoseImageService = &PoseImageGorm{}

type PoseImageGorm struct {
	db *gorm.DB
}

type PoseImageService interface {
	AddPoseImage(file *multipart.FileHeader) (*PoseImageTable, error)
	GetPoseImageById(id uint) (*PoseImageTable, error)
	GetPoseImageResById(id uint) (*PoseImageRes, error)
	DeletePoseImageById(id uint) error
}

func NewPoseImageService(db *gorm.DB) PoseImageService {
	return &PoseImageGorm{db}
}

func (poseImageGorm *PoseImageGorm) AddPoseImage(file *multipart.FileHeader) (*PoseImageTable, error) {
	dir := filepath.Join(UploadPath, "poses", "images")
	if err := os.MkdirAll(dir, os.ModePerm); err != nil {
		log.Printf("create gallery dir error: %v\n", err)
		return nil, err
	}
	tx := poseImageGorm.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while uploading photo")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return nil, err
	}
	generated, err := rand.GetToken()
	if err != nil {
		log.Printf("error generating filename: %v\n", err)
		tx.Rollback()
		return nil, err
	}
	ext := filepath.Ext(file.Filename)
	filename := generated[:len(generated)-1] + ext
	image := PoseImageTable{
		Filename: filename,
		Url:      PoseImageServerUrl + filename,
	}
	if err := tx.Create(&image).Error; err != nil {
		log.Printf("create image error: %v\n", err)
		tx.Rollback()
		return nil, err
	}
	if err := saveFile(file, image.FilePath()); err != nil {
		tx.Rollback()
		return nil, err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return nil, err
	}
	return &image, nil
}

func (img *PoseImageTable) FilePath() string {
	return filepath.Join(UploadPath, "poses", "images", img.Filename)
}

func (poseImageGorm *PoseImageGorm) GetPoseImageById(id uint) (*PoseImageTable, error) {
	image := new(PoseImageTable)
	if err := poseImageGorm.db.Where("deleted_at IS NULL AND id = ?", id).First(&image).Error; err != nil {
		return nil, err
	}
	return image, nil
}

func (poseImageGorm *PoseImageGorm) GetPoseImageResById(id uint) (*PoseImageRes, error) {
	image := new(PoseImageTable)
	if err := poseImageGorm.db.Where("deleted_at IS NULL AND id = ?", id).First(&image).Error; err != nil {
		return nil, err
	}
	res := new(PoseImageRes)
	res.Filename = image.Filename
	res.Url = image.Url
	return res, nil
}

func GetPoseImageResById(id uint, db *gorm.DB) (*PoseImageRes, error) {
	image := new(PoseImageTable)
	if err := db.Where("deleted_at IS NULL AND id = ?", id).First(&image).Error; err != nil {
		return nil, err
	}
	res := new(PoseImageRes)
	res.Filename = image.Filename
	res.Url = image.Url
	return res, nil
}

func (poseImageGorm *PoseImageGorm) DeletePoseImageById(id uint) error {
	image, err := poseImageGorm.GetPoseImageById(id)
	if err != nil {
		return err
	}
	tx := poseImageGorm.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while deleting image")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return err
	}
	if err := tx.Where("id = ?", id).Unscoped().Delete(&PoseImageTable{}).Error; err != nil {
		log.Printf("Fail deleting image in DB: %v\n", err)
		tx.Rollback()
		return err
	}
	err = os.Remove(image.FilePath())
	if err != nil {
		log.Printf("Fail deleting image: %v\n", err)
		tx.Rollback()
		return err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return err
	}
	return nil
}
