package models

import (
	"log"
	"time"

	"github.com/jinzhu/gorm"
)

type PoseTable struct {
	gorm.Model
	Name        string
	PoseImageID uint
	PoseVdoID   uint
}

func (PoseTable) TableName() string {
	return "poses"
}

type PoseRes struct {
	ID          uint         `json:"id"`
	Name        string       `json:"name"`
	Image       PoseImageRes `json:"image"`
	Vdo         PoseVdoRes   `json:"vdo"`
	Description []PoseDesRes `json:"descriptions"`
	UpdatedAt   time.Time    `json:"updated_at"`
	CreatedAt   time.Time    `json:"created_at"`
}

type PoseDesReq struct {
	Step        uint
	Description string
}

type CreatePoseReq struct {
	Name         string
	Descriptions []PoseDesReq
}

var _ PoseService = &PoseGorm{}

type PoseGorm struct {
	db *gorm.DB
}

type PoseService interface {
	CreatePose(name string) (*PoseTable, error)
	GetAllPoses() (*[]PoseTable, error)
	GetPoseById(id uint) (*PoseTable, error)
	GetPoseResById(id uint) (*PoseRes, error)
	UpdatePose(id uint, data map[string]interface{}) error
}

func NewPoseService(db *gorm.DB) PoseService {
	return &PoseGorm{db}
}

func (poseGorm *PoseGorm) CreatePose(name string) (*PoseTable, error) {
	tx := poseGorm.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while creating user")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return nil, err
	}
	pose := new(PoseTable)
	pose.Name = name
	if err := tx.Create(pose).Error; err != nil {
		log.Printf("create user error: %v\n", err)
		tx.Rollback()
		return nil, err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return nil, err
	}
	return pose, nil
}

func (pg *PoseGorm) GetAllPoses() (*[]PoseTable, error) {
	poses := new([]PoseTable)
	if err := pg.db.Where("deleted_at IS NULL").Find(poses).Error; err != nil {
		return nil, err
	}
	return poses, nil
}

func (poseGorm *PoseGorm) GetPoseResById(id uint) (*PoseRes, error) {
	pose := new(PoseTable)
	if err := poseGorm.db.Where("deleted_at IS NULL AND id = ?", id).First(pose).Error; err != nil {
		return nil, err
	}
	image, err := GetPoseImageResById(pose.PoseImageID, poseGorm.db)
	if err != nil {
		return nil, err
	}
	vdo, err := GetPoseVdoResById(pose.PoseVdoID, poseGorm.db)
	if err != nil {
		return nil, err
	}
	descriptions, err := GetPoseDesResByPoseId(pose.ID, poseGorm.db)
	if err != nil {
		return nil, err
	}
	res := new(PoseRes)
	res.ID = pose.ID
	res.Name = pose.Name
	res.Image = *image
	res.Vdo = *vdo
	res.Description = *descriptions
	res.UpdatedAt = pose.UpdatedAt
	res.CreatedAt = pose.CreatedAt
	return res, nil
}

func GetPoseResById(id uint, db *gorm.DB) (*PoseRes, error) {
	pose := new(PoseTable)
	if err := db.Where("deleted_at IS NULL AND id = ?", id).First(pose).Error; err != nil {
		return nil, err
	}
	image, err := GetPoseImageResById(pose.PoseImageID, db)
	if err != nil {
		return nil, err
	}
	vdo, err := GetPoseVdoResById(pose.PoseVdoID, db)
	if err != nil {
		return nil, err
	}
	descriptions, err := GetPoseDesResByPoseId(pose.ID, db)
	if err != nil {
		return nil, err
	}
	res := new(PoseRes)
	res.ID = pose.ID
	res.Name = pose.Name
	res.Image = *image
	res.Vdo = *vdo
	res.Description = *descriptions
	res.UpdatedAt = pose.UpdatedAt
	res.CreatedAt = pose.CreatedAt
	return res, nil
}

func (poseGorm *PoseGorm) GetPoseById(id uint) (*PoseTable, error) {
	pose := new(PoseTable)
	if err := poseGorm.db.Where("deleted_at IS NULL AND id = ?", id).First(pose).Error; err != nil {
		return nil, err
	}
	return pose, nil
}

func (poseGorm *PoseGorm) UpdatePose(id uint, data map[string]interface{}) error {
	tx := poseGorm.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while creating user")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return err
	}
	if err := tx.Model(&PoseTable{}).Where("id = ?", id).Update(data).Error; err != nil {
		log.Printf("cannot update pose data")
		return err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return err
	}
	return nil
}
