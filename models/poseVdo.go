package models

import (
	"log"
	"mime/multipart"
	"os"
	"path/filepath"
	"yoga-api/rand"

	"github.com/jinzhu/gorm"
)

const PoseVdoServerUrl = "https://yoga-api.twilightparadox.com/poses/vdos/"

type PoseVdoTable struct {
	gorm.Model
	Filename string `gorm:"NOT NULL"`
	Url      string `gorm:"NOT NULL"`
}

type PoseVdoRes struct {
	Filename string `json:"filename"`
	Url      string `json:"url"`
}

func (PoseVdoTable) TableName() string {
	return "posesVdo"
}

var _ PoseVdoService = &PoseVdoGorm{}

type PoseVdoGorm struct {
	db *gorm.DB
}

type PoseVdoService interface {
	AddPoseVdo(file *multipart.FileHeader) (*PoseVdoTable, error)
	GetPoseVdoById(id uint) (*PoseVdoTable, error)
	GetPoseVdoResById(id uint) (*PoseVdoRes, error)
	DeletePoseVdoById(id uint) error
}

func NewPoseVdoService(db *gorm.DB) PoseVdoService {
	return &PoseVdoGorm{db}
}

func (poseVdoGorm *PoseVdoGorm) AddPoseVdo(file *multipart.FileHeader) (*PoseVdoTable, error) {
	dir := filepath.Join(UploadPath, "poses", "vdos")
	if err := os.MkdirAll(dir, os.ModePerm); err != nil {
		log.Printf("create gallery dir error: %v\n", err)
		return nil, err
	}
	tx := poseVdoGorm.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while uploading vdo")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return nil, err
	}
	generated, err := rand.GetToken()
	if err != nil {
		log.Printf("error generating filename: %v\n", err)
		tx.Rollback()
		return nil, err
	}
	ext := filepath.Ext(file.Filename)
	filename := generated[:len(generated)-1] + ext
	vdo := PoseVdoTable{
		Filename: filename,
		Url:      PoseVdoServerUrl + filename,
	}
	if err := tx.Create(&vdo).Error; err != nil {
		log.Printf("create vdo error: %v\n", err)
		tx.Rollback()
		return nil, err
	}
	if err := saveFile(file, vdo.FilePath()); err != nil {
		tx.Rollback()
		return nil, err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return nil, err
	}
	return &vdo, nil
}

func (img *PoseVdoTable) FilePath() string {
	return filepath.Join(UploadPath, "poses", "vdos", img.Filename)
}

func (poseVdoGorm *PoseVdoGorm) GetPoseVdoById(id uint) (*PoseVdoTable, error) {
	vdo := new(PoseVdoTable)
	if err := poseVdoGorm.db.Where("deleted_at IS NULL AND id = ?", id).First(&vdo).Error; err != nil {
		return nil, err
	}
	return vdo, nil
}

func (poseVdoGorm *PoseVdoGorm) GetPoseVdoResById(id uint) (*PoseVdoRes, error) {
	vdo := new(PoseVdoTable)
	if err := poseVdoGorm.db.Where("deleted_at IS NULL AND id = ?", id).First(&vdo).Error; err != nil {
		return nil, err
	}
	res := new(PoseVdoRes)
	res.Filename = vdo.Filename
	res.Url = vdo.Url
	return res, nil
}

func GetPoseVdoResById(id uint, db *gorm.DB) (*PoseVdoRes, error) {
	vdo := new(PoseVdoTable)
	if err := db.Where("deleted_at IS NULL AND id = ?", id).First(&vdo).Error; err != nil {
		return nil, err
	}
	res := new(PoseVdoRes)
	res.Filename = vdo.Filename
	res.Url = vdo.Url
	return res, nil
}

func (poseVdoGorm *PoseVdoGorm) DeletePoseVdoById(id uint) error {
	vdo, err := poseVdoGorm.GetPoseVdoById(id)
	if err != nil {
		return err
	}
	tx := poseVdoGorm.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while deleting vdo")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return err
	}
	if err := tx.Where("id = ?", id).Unscoped().Delete(&PoseImageTable{}).Error; err != nil {
		log.Printf("Fail deleting vdo in DB: %v\n", err)
		tx.Rollback()
		return err
	}
	err = os.Remove(vdo.FilePath())
	if err != nil {
		log.Printf("Fail deleting image: %v\n", err)
		tx.Rollback()
		return err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return err
	}
	return nil
}
