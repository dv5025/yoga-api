package models

import (
	"log"

	"github.com/jinzhu/gorm"
)

// Reset will drop and re-create table
func Reset(db *gorm.DB) error {
	err := db.DropTableIfExists(
		&UserTable{},
		&ProfileImageTable{},
		&YogaClassTable{},
		&YogaClassCoverImgTable{},
		&PoseTable{},
		&PoseImageTable{},
		&PoseVdoTable{},
		&PoseDescriptionTable{},
		&YogaClassLevelTable{},
		&DefinedPoseTable{},
		&HistoryTable{},
		&PoseTable{},
		&CommentTable{},
		&PostTable{},
		&PostImgTable{},
	).Error
	if err != nil {
		log.Println(err)
		return err
	}

	return AutoMigrate(db)
}

// AutoMigrate will create or update table
func AutoMigrate(db *gorm.DB) error {
	return db.AutoMigrate(
		&UserTable{},
		&ProfileImageTable{},
		&YogaClassTable{},
		&YogaClassCoverImgTable{},
		&PoseTable{},
		&PoseImageTable{},
		&PoseVdoTable{},
		&PoseDescriptionTable{},
		&YogaClassLevelTable{},
		&DefinedPoseTable{},
		&HistoryTable{},
		&PoseTable{},
		&CommentTable{},
		&PostTable{},
		&PostImgTable{},
	).Error
}
