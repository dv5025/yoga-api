package models

import (
	"io"
	"log"
	"mime/multipart"
	"os"
)

const UploadPath = "upload"

func saveFile(file *multipart.FileHeader, dst string) error {
	src, err := file.Open()
	if err != nil {
		log.Printf("saveFile - open file error: %v\n", err)
		return err
	}
	defer src.Close()

	out, err := os.Create(dst)
	if err != nil {
		log.Printf("saveFile - create destination file error: %v\n", err)
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, src)
	if err != nil {
		log.Printf("saveFile - copy file error: %v\n", err)
	}
	return err
}
