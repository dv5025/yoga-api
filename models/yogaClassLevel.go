package models

import "github.com/jinzhu/gorm"

type YogaClassLevelTable struct {
	gorm.Model
	YogaClassID uint   `gorm:"NOT NULL"`
	LevelName   string `gorm:"NOT NULL"`
	Title       string `gorm:"NOT NULL"`
	Description string `gorm:"NOT NULL"`
}

func (YogaClassLevelTable) TableName() string {
	return "yogaClassLevel"
}

var _ YogaClassLevelService = &YogaClassLevelGorm{}

type YogaClassLevelGorm struct {
	db *gorm.DB
}

type YogaClassLevelShortRes struct {
	ID          uint   `json:"id"`
	LevelName   string `json:"levelName"`
	Title       string `json:"title"`
	Description string `json:"description"`
}

type YogaClassLevelRes struct {
	ID          uint              `json:"id"`
	YogaClass   YogaClassShortRes `json:"yogaClass"`
	LevelName   string            `json:"levelName"`
	Title       string            `json:"title"`
	Description string            `json:"description"`
	Poses       []DefinedPoseRes  `json:"poses"`
}

type YogaClassLevelService interface {
	GetYogaClassLevelByYogaClassId(yogaClassId uint) (*[]YogaClassLevelTable, error)
	GetYogaClassLevelById(id uint) (*YogaClassLevelTable, error)
}

func NewYogaClassLevelService(db *gorm.DB) YogaClassLevelService {
	return &YogaClassLevelGorm{db}
}

func (yclG *YogaClassLevelGorm) GetYogaClassLevelByYogaClassId(yogaClassId uint) (*[]YogaClassLevelTable, error) {
	yogaClassLevels := new([]YogaClassLevelTable)
	if err := yclG.db.Where("yoga_class_id = ?", yogaClassId).Find(yogaClassLevels).Error; err != nil {
		return nil, err
	}
	return yogaClassLevels, nil
}

func (yclG *YogaClassLevelGorm) GetYogaClassLevelById(id uint) (*YogaClassLevelTable, error) {
	yogaClassLevels := new(YogaClassLevelTable)
	if err := yclG.db.Where("deleted_at IS NULL AND id = ?", id).First(yogaClassLevels).Error; err != nil {
		return nil, err
	}
	return yogaClassLevels, nil
}
