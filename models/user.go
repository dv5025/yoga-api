package models

import (
	"errors"
	"log"
	"yoga-api/hash"
	"yoga-api/rand"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

const hmacKey = "secret"

type UserTable struct {
	gorm.Model
	Username  string `gorm:"UNIQUE_INDEX;NOT NULL" json:"username"`
	Email     string `gorm:"UNIQUE_INDEX;NOT NULL" json:"email"`
	Password  string `gorm:"NOT NULL" json:"password"`
	Firstname string `gorm:"NOT NULL" json:"firstname"`
	Lastname  string `gorm:"NOT NULL" json:"lastname"`
	Token     string `gorm:"UNIQUE_INDEX;NOT NULL" json:"token"`
}

func (UserTable) TableName() string {
	return "users"
}

type UserShortRes struct {
	Username     string          `json:"username"`
	ProfileImage ProfileImageRes `json:"profileImage"`
}

type CreateUserReq struct {
	Username  string
	Email     string
	Password  string
	Firstname string
	Lastname  string
}

type LoginReq struct {
	EmailOrUsername string
	Password        string
}

type EditProfileReq struct {
	Firstname string
	Lastname  string
}

type EditPasswordReq struct {
	Password string
}

type UserRes struct {
	ID           uint   `json:"id"`
	Username     string `json:"username"`
	Email        string `json:"email"`
	Firstname    string `json:"firstname"`
	Lastname     string `json:"lastname"`
	ProfileImage string `json:"profileImage"`
}

type UserService interface {
	CreateUser(data *CreateUserReq) error
	EditProfile(userID uint, data *EditProfileReq) error
	EditPassword(userID uint, password string) error
	Login(loginReq *LoginReq) (string, error) // return token anf error
	Logout(userID uint) error
	GetUserByToken(token string) (*UserTable, error)
	GetUserById(id uint) (*UserTable, error)
	GetUserShortResById(id uint) (*UserShortRes, error)
	CheckUsernameAvailability(username string) (bool, error)
	CheckEmailAvailability(email string) (bool, error)
}

var _ UserService = &UserGorm{}

type UserGorm struct {
	db   *gorm.DB
	hmac *hash.HMAC
}

func NewUserService(db *gorm.DB, hmac *hash.HMAC) UserService {
	return &UserGorm{db, hmac}
}

func (ug *UserGorm) CreateUser(data *CreateUserReq) error {
	// hash passwrod
	passHash, err := bcrypt.GenerateFromPassword([]byte(data.Password), bcrypt.MinCost)
	if err != nil {
		return err
	}

	// create token
	token, err := rand.GetToken()
	if err != nil {
		return err
	}

	// hmac
	tokenHashStr, err := ug.hmac.Hash(token)
	if err != nil {
		return err
	}

	user := new(UserTable)
	user.Username = data.Username
	user.Email = data.Email
	user.Password = string(passHash)
	user.Firstname = data.Firstname
	user.Lastname = data.Lastname
	user.Token = tokenHashStr

	tx := ug.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while creating user")
			tx.Rollback()
		}
	}()

	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return err
	}

	if err := tx.Create(user).Error; err != nil {
		log.Printf("create user error: %v\n", err)
		tx.Rollback()
		return err
	}

	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return err
	}

	return nil
}

func (ug *UserGorm) Login(loginReq *LoginReq) (string, error) {
	user := new(UserTable)
	if err := ug.db.Where("email = ? OR username = ?", loginReq.EmailOrUsername, loginReq.EmailOrUsername).First(user).Error; err != nil {
		return "", errors.New("invalid email or username")
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(loginReq.Password)); err != nil {
		return "", errors.New("invalid password")
	}

	// create token
	token, err := rand.GetToken()
	if err != nil {
		return "", err
	}

	// hmac
	tokenHashStr, err := ug.hmac.Hash(token)
	if err != nil {
		return "", err
	}

	tx := ug.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while logging in")
			tx.Rollback()
		}
	}()

	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return "", err
	}

	if err := ug.db.Model(user).Update("token", tokenHashStr).Error; err != nil {
		log.Printf("login error: %v\n", err)
		tx.Rollback()
		return "", err
	}

	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return "", err
	}

	return token, nil
}

func (ug *UserGorm) GetUserByToken(token string) (*UserTable, error) {
	// hmac
	tokenHashStr, err := ug.hmac.Hash(token)
	if err != nil {
		return nil, err
	}

	user := new(UserTable)
	if err := ug.db.Where("token = ?", tokenHashStr).First(user).Error; err != nil {
		return nil, err
	}
	return user, nil
}

func (ug *UserGorm) Logout(userID uint) error {
	// create token
	token, err := rand.GetToken()
	if err != nil {
		return err
	}
	// hmac
	tokenHashStr, err := ug.hmac.Hash(token)
	if err != nil {
		return err
	}
	tx := ug.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while logging out")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return err
	}
	if err := tx.Model(new(UserTable)).Where("id = ?", userID).Update("token", tokenHashStr).Error; err != nil {
		log.Printf("update user token error: %v\n", err)
		tx.Rollback()
		return err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return err
	}
	return nil
}

func (ug *UserGorm) EditProfile(userID uint, data *EditProfileReq) error {
	tx := ug.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while updating user's profile")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return err
	}
	if err := tx.Model(&UserTable{}).Where("id = ?", userID).Update(map[string]interface{}{"firstname": data.Firstname, "lastname": data.Lastname}).Error; err != nil {
		log.Printf("update user's profile error: %v\n", err)
		tx.Rollback()
		return err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return err
	}
	return nil
}

func (ug *UserGorm) EditPassword(userID uint, password string) error {
	tx := ug.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while updating user's password")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return err
	}
	// hash passwrod
	passHash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	if err != nil {
		return err
	}
	user := new(UserTable)
	if err := tx.Model(user).Where("id = ?", userID).Update("password", string(passHash)).Error; err != nil {
		log.Printf("update user's password error: %v\n", err)
		tx.Rollback()
		return err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return err
	}
	return nil
}

func (ug *UserGorm) GetUserById(id uint) (*UserTable, error) {
	user := new(UserTable)
	if err := ug.db.Where("id = ?", id).First(user).Error; err != nil {
		return nil, err
	}
	return user, nil
}

func (ug *UserGorm) GetUserShortResById(id uint) (*UserShortRes, error) {
	user := new(UserTable)
	if err := ug.db.Where("id = ?", id).First(user).Error; err != nil {
		return nil, err
	}
	profileImage := new(ProfileImageRes)
	image := new(ProfileImageTable)
	err := ug.db.Where("user_id = ?", user.ID).First(image).Error
	if err == nil {
		profileImage.Filename = image.Filename
		profileImage.Url = image.Url
	} else if err.Error() == "record not found" {
		profileImage.Filename = ""
		profileImage.Url = ""
	} else if err != nil {
		return nil, err
	}
	res := new(UserShortRes)
	res.ProfileImage = *profileImage
	res.Username = user.Username
	return res, nil
}

func (ug *UserGorm) CheckUsernameAvailability(username string) (bool, error) {
	user := new(UserTable)
	if err := ug.db.Where("username = ?", username).First(user).Error; err != nil {
		return true, err
	}
	return false, nil
}

func (ug *UserGorm) CheckEmailAvailability(email string) (bool, error) {
	user := new(UserTable)
	if err := ug.db.Where("email = ?", email).First(user).Error; err != nil {
		return true, err
	}
	return false, nil
}
