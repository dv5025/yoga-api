package models

import (
	"log"
	"time"

	"github.com/jinzhu/gorm"
)

type CommentTable struct {
	gorm.Model
	UserID  uint   `gorm:"NOT NULL"`
	PostID  uint   `gorm:"NOT NULL"`
	Message string `gorm:"NOT NULL"`
}

func (CommentTable) TableName() string {
	return "comment"
}

type AddCommentReq struct {
	Message string
	PostID  uint
}

type CommentRes struct {
	User      UserShortRes `json:"user"`
	Message   string       `json:"message"`
	CreatedAt time.Time    `json:"created_at"`
	UpdatedAt time.Time    `json:"updated_at"`
}

var _ CommentService = &CommentGorm{}

type CommentGorm struct {
	db *gorm.DB
}

type CommentService interface {
	AddComment(userID uint, postID uint, message string) (*CommentTable, error)
	GetCommentResByPostId(postID uint) (*[]CommentRes, error)
	DeleteCommentByPostId(postID uint) error
}

func NewCommentService(db *gorm.DB) CommentService {
	return &CommentGorm{db}
}

func (cg *CommentGorm) AddComment(userID uint, postID uint, message string) (*CommentTable, error) {
	comment := new(CommentTable)
	comment.UserID = userID
	comment.PostID = postID
	comment.Message = message
	tx := cg.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while adding comment")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return nil, err
	}
	if err := tx.Create(comment).Error; err != nil {
		log.Printf("add comment error: %v\n", err)
		tx.Rollback()
		return nil, err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return nil, err
	}
	return comment, nil
}

func (cg *CommentGorm) GetCommentResByPostId(postID uint) (*[]CommentRes, error) {
	comments := new([]CommentTable)
	if err := cg.db.Where("post_id = ?", postID).Order("created_at desc").Find(comments).Error; err != nil {
		return nil, err
	}
	res := []CommentRes{}
	for _, comment := range *comments {
		user := new(UserTable)
		if err := cg.db.Where("id = ?", comment.UserID).First(user).Error; err != nil {
			return nil, err
		}
		profileImage := new(ProfileImageRes)
		image := new(ProfileImageTable)
		err := cg.db.Where("user_id = ?", user.ID).First(image).Error
		if err == nil {
			profileImage.Filename = image.Filename
			profileImage.Url = image.Url
		} else if err.Error() == "record not found" {
			profileImage.Filename = ""
			profileImage.Url = ""
		} else if err != nil {
			return nil, err
		}
		temp := new(CommentRes)
		temp.User = UserShortRes{
			ProfileImage: *profileImage,
			Username:     user.Username,
		}
		temp.Message = comment.Message
		temp.CreatedAt = comment.CreatedAt
		temp.UpdatedAt = comment.UpdatedAt
		res = append(res, *temp)
	}
	return &res, nil
}

func (cg *CommentGorm) DeleteCommentByPostId(postID uint) error {
	tx := cg.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while deleting comment")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return err
	}
	if err := tx.Where("post_id = ?", postID).Unscoped().Delete(&CommentTable{}).Error; err != nil {
		log.Printf("Fail delete comment in DB: %v\n", err)
		tx.Rollback()
		return err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return err
	}
	return nil
}
