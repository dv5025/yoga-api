package models

import (
	"log"
	"time"

	"github.com/jinzhu/gorm"
)

type PostTable struct {
	gorm.Model
	UserID      uint `gorm:"NOT NULL"`
	PostImgID   uint
	Title       string `gorm:"NOT NULL"`
	Description string
}

func (PostTable) TableName() string {
	return "posts"
}

type PostRes struct {
	ID          uint         `json:"id"`
	User        UserShortRes `json:"user"`
	Image       PostImgRes   `json:"image"`
	Title       string       `json:"title"`
	Description string       `json:"description"`
	Comments    []CommentRes `json:"comments"`
	CreatedAt   time.Time    `json:"created_at"`
	UpdatedAt   time.Time    `json:"updated_at"`
}

var _ PostService = &PostGorm{}

type PostGorm struct {
	db *gorm.DB
}

type PostService interface {
	CreatePost(userID uint, title string, description string, imageID uint) (*PostTable, error)
	GetAllPosts() (*[]PostTable, error)
	GetPostById(id uint) (*PostTable, error)
	GetPostsByUserId(userID uint) (*[]PostTable, error)
	EditPost(postID uint, title string, description string, imageID uint) error
	DeletePostById(id uint) error
}

func NewPostService(db *gorm.DB) PostService {
	return &PostGorm{db}
}

func (pg *PostGorm) GetAllPosts() (*[]PostTable, error) {
	posts := new([]PostTable)
	if err := pg.db.Where("deleted_at IS NULL").Order("created_at desc").Find(posts).Error; err != nil {
		return nil, err
	}
	return posts, nil
}

func (pg *PostGorm) GetPostById(id uint) (*PostTable, error) {
	posts := new(PostTable)
	if err := pg.db.Where("deleted_at IS NULL AND id = ?", id).First(posts).Error; err != nil {
		return nil, err
	}
	return posts, nil
}

func (pg *PostGorm) GetPostsByUserId(userID uint) (*[]PostTable, error) {
	posts := new([]PostTable)
	if err := pg.db.Where("deleted_at IS NULL AND user_id = ?", userID).Order("created_at desc").Find(posts).Error; err != nil {
		return nil, err
	}
	return posts, nil
}

func (pg *PostGorm) CreatePost(userID uint, title string, description string, imageID uint) (*PostTable, error) {
	post := new(PostTable)
	post.UserID = userID
	post.PostImgID = imageID
	post.Title = title
	post.Description = description
	tx := pg.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while creating post")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return nil, err
	}
	if err := tx.Create(post).Error; err != nil {
		log.Printf("create post error: %v\n", err)
		tx.Rollback()
		return nil, err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return nil, err
	}
	return post, nil
}

func (pg *PostGorm) EditPost(postID uint, title string, description string, imageID uint) error {
	tx := pg.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while updating post")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return err
	}
	if err := tx.Model(&PostTable{}).Where("id = ?", postID).Updates(map[string]interface{}{"title": title, "description": description, "post_img_id": imageID}).Error; err != nil {
		tx.Rollback()
		return err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return err
	}
	return nil
}

func (pg *PostGorm) DeletePostById(id uint) error {
	tx := pg.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while deleting post")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return err
	}
	if err := tx.Where("id = ?", id).Unscoped().Delete(&PostTable{}).Error; err != nil {
		log.Printf("Fail delete post in DB: %v\n", err)
		tx.Rollback()
		return err
	}
	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return err
	}
	return nil
}
