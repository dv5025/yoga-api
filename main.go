package main

import (
	"log"
	"yoga-api/config"
	"yoga-api/handlers"
	"yoga-api/hash"
	"yoga-api/middleware"
	"yoga-api/models"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
	conf := config.Load()

	db, err := gorm.Open("mysql", conf.Connection)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	if conf.Mode == "dev" {
		db.LogMode(true)
	}

	err = models.AutoMigrate(db)
	if err != nil {
		log.Fatal(err)
	}

	router := gin.Default()
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	config.AllowHeaders = []string{"Origin", "Content-Length", "Content-Type", "Authorization"}
	router.Use(cors.New(config))

	hmac := hash.NewHMAC(conf.HMACKey)

	userService := models.NewUserService(db, hmac)
	proImgService := models.NewProfileImageService(db)
	yogaClassService := models.NewYogaClassService(db)
	yogaClassCoverImgService := models.NewYogaClassCoverImgService(db)
	yogaClassLevelService := models.NewYogaClassLevelService(db)
	poseImageService := models.NewPoseImageService(db)
	poseVdoService := models.NewPoseVdoService(db)
	poseDescriptionService := models.NewPoseDescriptionService(db)
	poseService := models.NewPoseService(db)
	definedPoseService := models.NewDefinedPoseService(db)
	historyService := models.NewHistoryService(db)
	postSevice := models.NewPostService(db)
	postImageService := models.NewPostImgService(db)
	commentService := models.NewCommentService(db)

	userHandler := handlers.NewUserHandler(userService, proImgService)
	proImgHandler := handlers.NewProfileImageHandler(proImgService)
	yogaClassHandler := handlers.NewYogaClassHandler(yogaClassService, yogaClassCoverImgService, yogaClassLevelService, definedPoseService)
	poseHandler := handlers.NewPoseHandler(poseService, poseImageService, poseVdoService, poseDescriptionService)
	yogaClassLevelHandler := handlers.NewYogaClassLevelHandler(yogaClassService, yogaClassLevelService, poseService, definedPoseService)
	historyHandler := handlers.NewHistpryHandler(historyService, yogaClassService, yogaClassLevelService, definedPoseService)
	postHandler := handlers.NewPostHandler(postSevice, userService, commentService, postImageService)

	authorized := router.Group("/")
	authorized.Use(middleware.Auth(userService))
	{
		authorized.POST("/auth/logout", userHandler.Logout)
		authorized.GET("/user/profile", userHandler.GetProfile)
		authorized.PATCH("/user/profile", userHandler.EditProfile)
		authorized.PATCH("/user/password", userHandler.EditPassword)

		authorized.PATCH("/user/profileImage", proImgHandler.EditProfileImage)

		authorized.POST("/yogaClass", yogaClassHandler.CreateYogaClass)
		authorized.GET("/yogaClasses", yogaClassHandler.GetAllPresetYogaClass)
		authorized.GET("/yogaClasses/:id", yogaClassHandler.GetPresetYogaClassById)
		authorized.GET("/customClasses/:id", yogaClassHandler.GetCustomYogaClassById)
		authorized.GET("/customClass/user", yogaClassHandler.GetCustomYogaClassByUserId)
		authorized.POST("/customClass", yogaClassHandler.CreateCustomClass)

		authorized.GET("/yogaClass/level/:id", yogaClassLevelHandler.GetYogaClassLevelById)

		authorized.POST("/poses", poseHandler.CreatePose)
		authorized.POST("/poses/:id/image", poseHandler.UploadPoseImage)
		authorized.POST("/poses/:id/vdo", poseHandler.UploadPoseVdo)
		authorized.GET("/poses", poseHandler.GetAllPose)
		authorized.GET("/poses/:id", poseHandler.GetPoseById)

		authorized.GET("/posts", postHandler.GetAllPostsHandle)
		authorized.POST("/posts", postHandler.AddPost)
		authorized.PATCH("/posts", postHandler.EditPost)
		authorized.DELETE("/posts", postHandler.DeletePost)
		authorized.GET("/posts/:id", postHandler.GetPostById)
		authorized.GET("/post/user", postHandler.GetPostByUserId)
		authorized.POST("/comment", postHandler.AddComment)

		authorized.GET("/history", historyHandler.GetUserHistories)
		authorized.POST("/history", historyHandler.AddHistory)
	}

	router.POST("/auth/signup", userHandler.CreateUser)
	router.POST("/auth/login", userHandler.Login)
	router.GET("/auth/checkUsernameAvailability", userHandler.CheckUsernameAvailability)
	router.GET("/auth/checkEmailAvailability", userHandler.CheckEmailAvailability)

	router.Static("/posesAssets/images", "./upload/poses/images")
	router.Static("/posesAssets/vdos", "./upload/poses/vdos")
	router.Static("/user/profileImage", "./upload/profileImg")
	router.Static("/yogaClassCoverImg", "./upload/yogaCoverImages")
	router.Static("/postImg", "./upload/postImg")

	router.GET("/ping", func(c *gin.Context) {
		c.String(200, "Hello World")
	})

	router.Run()
}
