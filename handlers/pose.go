package handlers

import (
	"errors"
	"net/http"
	"strconv"
	"strings"
	"yoga-api/models"

	"github.com/gin-gonic/gin"
)

type PoseHandler struct {
	PoseService            models.PoseService
	PoseImageService       models.PoseImageService
	PoseVdoService         models.PoseVdoService
	PoseDescriptionService models.PoseDescriptionService
}

func NewPoseHandler(PoseService models.PoseService, PoseImageService models.PoseImageService, PoseVdoService models.PoseVdoService, PoseDescriptionService models.PoseDescriptionService) *PoseHandler {
	return &PoseHandler{PoseService, PoseImageService, PoseVdoService, PoseDescriptionService}
}

func (ph *PoseHandler) GetAllPose(c *gin.Context) {
	poses, err := ph.PoseService.GetAllPoses()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	posesRes := []models.PoseRes{}
	for _, pose := range *poses {
		image, err := ph.PoseImageService.GetPoseImageById(pose.PoseImageID)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		vdo, err := ph.PoseVdoService.GetPoseVdoById(pose.PoseVdoID)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		descriptions, err := ph.PoseDescriptionService.GetPoseDesByPoseId(pose.ID)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		desRes := GetDescriptionsRes(descriptions)
		poseRes := new(models.PoseRes)
		poseRes.ID = pose.ID
		poseRes.Name = pose.Name
		poseRes.Image = *GetPoseImgRes(image)
		poseRes.Vdo = *GetPoseVdoRes(vdo)
		poseRes.Description = *desRes
		poseRes.CreatedAt = pose.CreatedAt
		poseRes.UpdatedAt = pose.UpdatedAt
		posesRes = append(posesRes, *poseRes)
	}

	c.JSON(http.StatusOK, posesRes)
}

func (ph *PoseHandler) GetPoseById(c *gin.Context) {
	idStr := c.Param("id")
	idInt, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	pose, err := ph.PoseService.GetPoseById(uint(idInt))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	image, err := ph.PoseImageService.GetPoseImageById(pose.PoseImageID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	vdo, err := ph.PoseVdoService.GetPoseVdoById(pose.PoseVdoID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	descriptions, err := ph.PoseDescriptionService.GetPoseDesByPoseId(pose.ID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	desRes := GetDescriptionsRes(descriptions)
	poseRes := models.PoseRes{}
	poseRes.ID = pose.ID
	poseRes.Name = pose.Name
	poseRes.Image = *GetPoseImgRes(image)
	poseRes.Vdo = *GetPoseVdoRes(vdo)
	poseRes.Description = *desRes
	poseRes.CreatedAt = pose.CreatedAt
	poseRes.UpdatedAt = pose.UpdatedAt

	c.JSON(http.StatusOK, poseRes)
}

func (poseHandler *PoseHandler) CreatePose(c *gin.Context) {
	_, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": "unauthorized",
		})
		return
	}
	data := new(models.CreatePoseReq)
	if err := c.BindJSON(data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	pose, err := poseHandler.PoseService.CreatePose(data.Name)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	descriptions := []models.PoseDescriptionTable{}
	for _, des := range data.Descriptions {
		createDesReq := new(models.CreatePoseDesReq)
		createDesReq.PoseID = pose.ID
		createDesReq.Step = des.Step
		createDesReq.Description = des.Description
		poseDes, err := poseHandler.PoseDescriptionService.CreatePoseDescription(createDesReq)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		descriptions = append(descriptions, *poseDes)
	}
	c.JSON(http.StatusBadRequest, gin.H{
		"id":           pose.ID,
		"name":         pose.Name,
		"descriptions": descriptions,
	})
}

func (poseHandler *PoseHandler) UploadPoseImage(c *gin.Context) {
	_, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": "unauthorized",
		})
		return
	}
	if err := validateUploadPoseImage(c); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	poseIdInt, _ := strconv.Atoi(c.Param("id"))
	pose, err := poseHandler.PoseService.GetPoseById(uint(poseIdInt))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	form, _ := c.MultipartForm()
	image, err := poseHandler.PoseImageService.AddPoseImage(form.File["image"][0])
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	if err := poseHandler.PoseService.UpdatePose(pose.ID, map[string]interface{}{"pose_image_id": image.ID}); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, gin.H{
		"message": "image_uploaded",
	})
}

func (poseHandler *PoseHandler) UploadPoseVdo(c *gin.Context) {
	_, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": "unauthorized",
		})
		return
	}
	if err := validateUploadPoseVdo(c); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	poseIdInt, _ := strconv.Atoi(c.Param("id"))
	pose, err := poseHandler.PoseService.GetPoseById(uint(poseIdInt))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	form, _ := c.MultipartForm()
	vdo, err := poseHandler.PoseVdoService.AddPoseVdo(form.File["vdo"][0])
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	if err := poseHandler.PoseService.UpdatePose(pose.ID, map[string]interface{}{"pose_vdo_id": vdo.ID}); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, gin.H{
		"message": "image_uploaded",
	})
}

func validateUploadPoseImage(c *gin.Context) error {
	if _, err := c.MultipartForm(); err != nil {
		return err
	}
	idStr := c.Param("id")
	if strings.Replace(idStr, " ", "", -1) == "" {
		return errors.New("id cannot be empty")
	}
	if _, err := strconv.Atoi(idStr); err != nil {
		return errors.New("id must be number")
	}
	form, _ := c.MultipartForm()
	if len(form.File["image"]) == 0 {
		return errors.New("image is required")
	}
	return nil
}

func validateUploadPoseVdo(c *gin.Context) error {
	if _, err := c.MultipartForm(); err != nil {
		return err
	}
	idStr := c.Param("id")
	if strings.Replace(idStr, " ", "", -1) == "" {
		return errors.New("id cannot be empty")
	}
	if _, err := strconv.Atoi(idStr); err != nil {
		return errors.New("id must be number")
	}
	form, _ := c.MultipartForm()
	if len(form.File["vdo"]) == 0 {
		return errors.New("image is required")
	}
	return nil
}
