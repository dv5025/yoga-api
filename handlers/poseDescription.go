package handlers

import "yoga-api/models"

func GetDescriptionsRes(data *[]models.PoseDescriptionTable) *[]models.PoseDesRes {
	desRes := []models.PoseDesRes{}
	for _, des := range *data {
		temp := new(models.PoseDesRes)
		temp.Step = des.Step
		temp.Description = des.Description
		desRes = append(desRes, *temp)
	}
	return &desRes
}
