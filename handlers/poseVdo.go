package handlers

import "yoga-api/models"

func GetPoseVdoRes(vdo *models.PoseVdoTable) *models.PoseVdoRes {
	res := new(models.PoseVdoRes)
	res.Filename = vdo.Filename
	res.Url = vdo.Url
	return res
}
