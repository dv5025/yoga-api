package handlers

import (
	"net/http"
	"strconv"
	"yoga-api/models"

	"github.com/gin-gonic/gin"
)

type PostHandler struct {
	postService      models.PostService
	userService      models.UserService
	commentService   models.CommentService
	postImageService models.PostImgService
}

func NewPostHandler(postService models.PostService, userService models.UserService, commentService models.CommentService, postImageService models.PostImgService) *PostHandler {
	return &PostHandler{postService, userService, commentService, postImageService}
}

func (ph *PostHandler) GetAllPostsHandle(c *gin.Context) {
	posts, err := ph.postService.GetAllPosts()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := []models.PostRes{}
	for _, post := range *posts {
		user, err := ph.userService.GetUserShortResById(post.UserID)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		image, err := ph.postImageService.GetPostImgResById(post.PostImgID)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		comments, err := ph.commentService.GetCommentResByPostId(post.ID)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		temp := new(models.PostRes)
		temp.ID = post.ID
		temp.User = *user
		temp.Image = *image
		temp.Title = post.Title
		temp.Description = post.Description
		temp.Comments = *comments
		temp.UpdatedAt = post.UpdatedAt
		temp.CreatedAt = post.CreatedAt
		res = append(res, *temp)
	}
	c.JSON(http.StatusOK, res)
}

func (ph *PostHandler) GetPostById(c *gin.Context) {
	idStr := c.Param("id")
	idInt, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	post, err := ph.postService.GetPostById(uint(idInt))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	user, err := ph.userService.GetUserShortResById(post.UserID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	image, err := ph.postImageService.GetPostImgResById(post.PostImgID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	comments, err := ph.commentService.GetCommentResByPostId(post.ID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := new(models.PostRes)
	res.ID = post.ID
	res.User = *user
	res.Image = *image
	res.Title = post.Title
	res.Description = post.Description
	res.Comments = *comments
	res.UpdatedAt = post.UpdatedAt
	res.CreatedAt = post.CreatedAt
	c.JSON(http.StatusOK, res)
}

func (ph *PostHandler) GetPostByUserId(c *gin.Context) {
	user, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": "unauthorized",
		})
		return
	}
	posts, err := ph.postService.GetPostsByUserId(user.ID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := []models.PostRes{}
	for _, post := range *posts {
		user, err := ph.userService.GetUserShortResById(post.UserID)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		image, err := ph.postImageService.GetPostImgResById(post.PostImgID)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		comments, err := ph.commentService.GetCommentResByPostId(post.ID)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		temp := new(models.PostRes)
		temp.ID = post.ID
		temp.User = *user
		temp.Image = *image
		temp.Title = post.Title
		temp.Description = post.Description
		temp.Comments = *comments
		temp.UpdatedAt = post.UpdatedAt
		temp.CreatedAt = post.CreatedAt
		res = append(res, *temp)
	}
	c.JSON(http.StatusOK, res)
}

func (ph *PostHandler) AddComment(c *gin.Context) {
	user, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": "unauthorized",
		})
		return
	}
	addCommentReq := new(models.AddCommentReq)
	if err := c.BindJSON(addCommentReq); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	if _, err := ph.commentService.AddComment(user.ID, addCommentReq.PostID, addCommentReq.Message); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": "comment_added",
	})
}

func (ph *PostHandler) AddPost(c *gin.Context) {
	user, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": "unauthorized",
		})
		return
	}
	title := c.PostForm("title")
	description := c.PostForm("description")
	form, err := c.MultipartForm()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	if len(form.File["image"]) == 0 {
		if _, err := ph.postService.CreatePost(user.ID, title, description, 0); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		c.JSON(http.StatusOK, gin.H{
			"message": "post_created",
		})
		return
	}
	image, err := ph.postImageService.AddPostImage(form.File["image"][0])
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	if _, err := ph.postService.CreatePost(user.ID, title, description, image.ID); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": "post_created",
	})
}

func (ph *PostHandler) EditPost(c *gin.Context) {
	postId := c.PostForm("postId")
	postIdInt, err := strconv.Atoi(postId)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	post, err := ph.postService.GetPostById(uint(postIdInt))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	title := c.PostForm("title")
	description := c.PostForm("description")
	form, err := c.MultipartForm()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	if len(form.File["image"]) == 0 {
		if err := ph.postService.EditPost(post.ID, title, description, 0); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		c.JSON(http.StatusOK, gin.H{
			"message": "post_updated",
		})
		return
	}
	if err := ph.postImageService.DeletePostImageById(post.PostImgID); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	image, err := ph.postImageService.AddPostImage(form.File["image"][0])
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	if err := ph.postService.EditPost(post.ID, title, description, image.ID); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": "post_updated",
	})
}

func (ph *PostHandler) DeletePost(c *gin.Context) {
	idStr := c.Query("postId")
	idInt, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	post, err := ph.postService.GetPostById(uint(idInt))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	if err := ph.postService.DeletePostById(post.ID); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	if post.PostImgID != 0 {
		if err := ph.postImageService.DeletePostImageById(post.PostImgID); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
	}
	if err := ph.commentService.DeleteCommentByPostId(post.ID); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": "post_deleted",
	})
}
