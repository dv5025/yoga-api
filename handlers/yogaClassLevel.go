package handlers

import (
	"net/http"
	"strconv"
	"yoga-api/models"

	"github.com/gin-gonic/gin"
)

type YogaClassLevelHandler struct {
	YogaClassService      models.YogaClassService
	YogaClassLevelService models.YogaClassLevelService
	PoseService           models.PoseService
	DefinedPoseService    models.DefinedPoseService
}

func NewYogaClassLevelHandler(YogaClassService models.YogaClassService, YogaClassLevelService models.YogaClassLevelService, PoseService models.PoseService, DefinedPoseService models.DefinedPoseService) *YogaClassLevelHandler {
	return &YogaClassLevelHandler{YogaClassService, YogaClassLevelService, PoseService, DefinedPoseService}
}

func (ylH *YogaClassLevelHandler) GetYogaClassLevelById(c *gin.Context) {
	idStr := c.Param("id")
	idInt, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	yogaClassLevel, err := ylH.YogaClassLevelService.GetYogaClassLevelById(uint(idInt))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	pose, err := ylH.DefinedPoseService.GetDefinedPoseResByLevelId(yogaClassLevel.ID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	yogaClass, err := ylH.YogaClassService.GetYogaClassShortResById(yogaClassLevel.YogaClassID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := new(models.YogaClassLevelRes)
	res.ID = yogaClassLevel.ID
	res.YogaClass = *yogaClass
	res.LevelName = yogaClassLevel.LevelName
	res.Title = yogaClassLevel.Title
	res.Description = yogaClassLevel.Description
	res.Poses = *pose
	c.JSON(http.StatusOK, res)
}

func GetYogaClassLevelsShortRes(data *[]models.YogaClassLevelTable) *[]models.YogaClassLevelShortRes {
	levelsRes := []models.YogaClassLevelShortRes{}
	for _, level := range *data {
		temp := new(models.YogaClassLevelShortRes)
		temp.ID = level.ID
		temp.LevelName = level.LevelName
		temp.Title = level.Title
		temp.Description = level.Description
		levelsRes = append(levelsRes, *temp)
	}
	return &levelsRes
}

func GetYogaClassLevelShortRes(data *models.YogaClassLevelTable) *models.YogaClassLevelShortRes {
	levelRes := models.YogaClassLevelShortRes{}
	levelRes.ID = data.ID
	levelRes.LevelName = data.LevelName
	levelRes.Title = data.Title
	levelRes.Description = data.Description
	return &levelRes
}
