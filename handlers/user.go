package handlers

import (
	"net/http"
	"regexp"
	"strings"
	"yoga-api/models"

	"github.com/gin-gonic/gin"
)

type UserHandler struct {
	us      models.UserService
	proImgS models.ProfileImageService
}

func NewUserHandler(us models.UserService, proImgS models.ProfileImageService) *UserHandler {
	return &UserHandler{us, proImgS}
}

func (uh *UserHandler) CreateUser(c *gin.Context) {
	validateCreateUser(c)
	user := new(models.CreateUserReq)
	user.Username = strings.Replace(c.PostForm("username"), " ", "", -1)
	user.Email = strings.Replace(c.PostForm("email"), " ", "", -1)
	user.Password = strings.Replace(c.PostForm("password"), " ", "", -1)
	user.Firstname = strings.Replace(c.PostForm("firstname"), " ", "", -1)
	user.Lastname = strings.Replace(c.PostForm("lastname"), " ", "", -1)

	// add to DB
	if err := uh.us.CreateUser(user); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, gin.H{
		"messsage": "user created",
	})
}

func (uh *UserHandler) Login(c *gin.Context) {
	loginReq := new(models.LoginReq)
	if err := c.BindJSON(loginReq); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	if strings.TrimSpace(loginReq.EmailOrUsername) == "" {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "email or username cannot be empty",
		})
		return
	}
	if strings.TrimSpace(loginReq.Password) == "" {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "password cannot be empty",
		})
		return
	}

	token, err := uh.us.Login(loginReq)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, gin.H{
		"token": token,
	})
}

func (uh *UserHandler) Logout(c *gin.Context) {
	user, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": "unauthorized",
		})
		return
	}
	if err := uh.us.Logout(user.ID); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": "logout successfully",
	})
}

func (uh *UserHandler) GetProfile(c *gin.Context) {
	user, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": "unauthorized",
		})
		return
	}

	profileImage, err := uh.proImgS.GetProfileImageByUserId(user.ID)
	if profileImage != nil {
		c.JSON(http.StatusOK, gin.H{
			"id":           user.ID,
			"username":     user.Username,
			"email":        user.Email,
			"firstname":    user.Firstname,
			"lastname":     user.Lastname,
			"profileImage": GetProfileImgRes(profileImage),
		})
		return
	} else if err.Error() == "record not found" {
		c.JSON(http.StatusOK, gin.H{
			"id":        user.ID,
			"username":  user.Username,
			"email":     user.Email,
			"firstname": user.Firstname,
			"lastname":  user.Lastname,
		})
		return
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
	}
}

func (uh *UserHandler) EditProfile(c *gin.Context) {
	user, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": "unauthorized",
		})
		return
	}
	editProfileReq := new(models.EditProfileReq)
	if err := c.BindJSON(editProfileReq); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	editProfileReq.Firstname = strings.Replace(editProfileReq.Firstname, " ", "", -1)
	editProfileReq.Lastname = strings.Replace(editProfileReq.Lastname, " ", "", -1)
	if editProfileReq.Firstname == "" {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "First name cannot be empty",
		})
		return
	}
	if editProfileReq.Lastname == "" {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "Last name cannot be empty",
		})
		return
	}
	if err := uh.us.EditProfile(user.ID, editProfileReq); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	user, err := uh.us.GetUserById(user.ID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	profileImage, err := uh.proImgS.GetProfileImageByUserId(user.ID)
	if profileImage != nil {
		c.JSON(http.StatusCreated, gin.H{
			"id":           user.ID,
			"username":     user.Username,
			"email":        user.Email,
			"firstname":    user.Firstname,
			"lastname":     user.Lastname,
			"profileImage": GetProfileImgRes(profileImage),
		})
		return
	} else if err.Error() == "record not found" {
		c.JSON(http.StatusCreated, gin.H{
			"id":        user.ID,
			"username":  user.Username,
			"email":     user.Email,
			"firstname": user.Firstname,
			"lastname":  user.Lastname,
		})
		return
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
	}
}

func (uh *UserHandler) EditPassword(c *gin.Context) {
	user, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": "unauthorized",
		})
		return
	}
	editPasswordReq := new(models.EditPasswordReq)
	if err := c.BindJSON(editPasswordReq); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	editPasswordReq.Password = strings.Replace(editPasswordReq.Password, " ", "", -1)
	if editPasswordReq.Password == "" {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "Password cannot be empty",
		})
		return
	}
	if len(editPasswordReq.Password) < 8 {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "Password must have at least 8 characters",
		})
		return
	}
	if err := uh.us.EditPassword(user.ID, editPasswordReq.Password); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, gin.H{
		"message": "password_updated",
	})
}

func (uh *UserHandler) CheckUsernameAvailability(c *gin.Context) {
	username := c.Query("username")
	usernameTrimSpace := strings.TrimSpace(username)
	if usernameTrimSpace == "" {
		c.JSON(http.StatusOK, gin.H{"message": "username param connot be empty"})
		return
	}
	available, err := uh.us.CheckUsernameAvailability(usernameTrimSpace)
	if !available {
		c.JSON(http.StatusOK, gin.H{"available": available})
		return
	} else if err.Error() == "record not found" {
		c.JSON(http.StatusOK, gin.H{"available": available})
		return
	}
	c.JSON(http.StatusBadRequest, gin.H{
		"message": err.Error(),
	})
}

func (uh *UserHandler) CheckEmailAvailability(c *gin.Context) {
	email := c.Query("email")
	emailTrimSpace := strings.TrimSpace(email)
	if emailTrimSpace == "" {
		c.JSON(http.StatusOK, gin.H{"message": "email param connot be empty"})
		return
	}
	available, err := uh.us.CheckEmailAvailability(emailTrimSpace)
	if !available {
		c.JSON(http.StatusOK, gin.H{"available": available})
		return
	} else if err.Error() == "record not found" {
		c.JSON(http.StatusOK, gin.H{"available": available})
		return
	}
	c.JSON(http.StatusBadRequest, gin.H{
		"message": err.Error(),
	})
}

func validateCreateUser(c *gin.Context) {
	if len(strings.Replace(c.PostForm("username"), " ", "", -1)) < 4 {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "Username must have at least 4 characters",
		})
		return
	}
	emailRegexp := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	if !emailRegexp.MatchString(c.PostForm("email")) {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "Email must in email format",
		})
		return
	}
	if len(strings.Replace(c.PostForm("password"), " ", "", -1)) < 8 {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "Password must have at least 8 characters",
		})
		return
	}
	if len(strings.Replace(c.PostForm("firstname"), " ", "", -1)) < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "First name must have at least 2 characters",
		})
		return
	}
	if len(strings.Replace(c.PostForm("lastname"), " ", "", -1)) < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "Last name must have at least 2 characters",
		})
		return
	}
}
