package handlers

import "yoga-api/models"

func GetYogaClassCoverImgRes(image *models.YogaClassCoverImgTable) *models.YogaClassCoverImgRes {
	res := new(models.YogaClassCoverImgRes)
	res.Filename = image.Filename
	res.Url = image.Url
	return res
}
