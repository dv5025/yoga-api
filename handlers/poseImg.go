package handlers

import "yoga-api/models"

func GetPoseImgRes(image *models.PoseImageTable) *models.PoseImageRes {
	res := new(models.PoseImageRes)
	res.Filename = image.Filename
	res.Url = image.Url
	return res
}
