package handlers

import (
	"errors"
	"net/http"
	"strconv"
	"strings"
	"yoga-api/models"

	"github.com/gin-gonic/gin"
)

type YogaClassHandler struct {
	YogaClassService         models.YogaClassService
	YogaClassCoverImgService models.YogaClassCoverImgService
	YogaClassLevelService    models.YogaClassLevelService
	DefinedPoseService       models.DefinedPoseService
}

func NewYogaClassHandler(YogaClassService models.YogaClassService, YogaClassCoverImgService models.YogaClassCoverImgService, YogaClassLevelService models.YogaClassLevelService, DefinedPoseService models.DefinedPoseService) *YogaClassHandler {
	return &YogaClassHandler{YogaClassService, YogaClassCoverImgService, YogaClassLevelService, DefinedPoseService}
}

func (yh *YogaClassHandler) CreateCustomClass(c *gin.Context) {
	user, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": "unauthorized",
		})
		return
	}
	createClassReq := new(models.CreateCustomClassReq)
	if err := c.BindJSON(createClassReq); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	yogaClass, err := yh.YogaClassService.CreateCustomClass(createClassReq.Name, createClassReq.Description, user.ID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	for index, pose := range createClassReq.Poses {
		if err := yh.DefinedPoseService.AddDefinedPose(yogaClass.ID, 0, pose.PoseID, pose.Time, uint(index)+1); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
	}
	c.JSON(http.StatusCreated, gin.H{
		"message": "class created",
	})
}

func (YogaClassHandler *YogaClassHandler) CreateYogaClass(c *gin.Context) {
	user, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": "unauthorized",
		})
		return
	}
	if err := validateCreateYoga(c); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	form, _ := c.MultipartForm()
	image, err := YogaClassHandler.YogaClassCoverImgService.CreateYogaClassCoverImg(form.File["image"][0])
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	isPublic, _ := strconv.ParseBool(c.PostForm("isPublic"))
	createYogalClassReq := new(models.CreateYogaClassReq)
	createYogalClassReq.Name = c.PostForm("name")
	createYogalClassReq.Descriotion = c.PostForm("description")
	createYogalClassReq.IsPublic = isPublic
	createYogalClassReq.UserID = user.ID
	createYogalClassReq.CoverImageID = image.ID
	yogalass, err := YogaClassHandler.YogaClassService.CreateYogoClass(createYogalClassReq)
	if err != nil {
		YogaClassHandler.YogaClassCoverImgService.DeleteYogaClassCoverImgById(image.ID)
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, yogalass)
}

func validateCreateYoga(c *gin.Context) error {
	form, err := c.MultipartForm()
	if err != nil {
		return err
	}
	if len(form.File["image"]) == 0 {
		return errors.New("Image is require")
	}
	if _, err := strconv.ParseBool(c.PostForm("isPublic")); err != nil {
		return err
	}
	if len(strings.Replace(c.PostForm("name"), " ", "", -1)) < 2 {
		return errors.New("Name must have at least 2 characters")
	}
	if len(strings.Replace(c.PostForm("description"), " ", "", -1)) < 10 {
		return errors.New("Description must have at least 10 characters")
	}
	return nil
}

func (ygch *YogaClassHandler) GetAllPresetYogaClass(c *gin.Context) {
	yogaClasses, err := ygch.YogaClassService.GetAllPresetYogaClass()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	yogaClassesRes := []models.YogaClassRes{}
	for _, yogaClass := range *yogaClasses {
		image, err := ygch.YogaClassCoverImgService.GetYogaClassCoverImgById(yogaClass.CoverImageID)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		yogaClassLevels, err := ygch.YogaClassLevelService.GetYogaClassLevelByYogaClassId(yogaClass.ID)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		levelsRes := GetYogaClassLevelsShortRes(yogaClassLevels)
		yogaClassRes := new(models.YogaClassRes)
		yogaClassRes.ID = yogaClass.ID
		yogaClassRes.Name = yogaClass.Name
		yogaClassRes.Image = *GetYogaClassCoverImgRes(image)
		yogaClassRes.Description = yogaClass.Descriotion
		yogaClassRes.Type = yogaClass.Type
		yogaClassRes.Levels = *levelsRes
		yogaClassRes.UpdatedAt = yogaClass.UpdatedAt
		yogaClassRes.CreatedAt = yogaClass.CreatedAt
		yogaClassesRes = append(yogaClassesRes, *yogaClassRes)
	}
	c.JSON(http.StatusOK, yogaClassesRes)
}

func (ygch *YogaClassHandler) GetPresetYogaClassById(c *gin.Context) {
	idStr := c.Param("id")
	idInt, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	yogaClass, err := ygch.YogaClassService.GetPresetYogaClassById(uint(idInt))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	image, err := ygch.YogaClassCoverImgService.GetYogaClassCoverImgById(yogaClass.CoverImageID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	yogaClassLevels, err := ygch.YogaClassLevelService.GetYogaClassLevelByYogaClassId(yogaClass.ID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	levelsRes := GetYogaClassLevelsShortRes(yogaClassLevels)
	yogaClassRes := new(models.YogaClassRes)
	yogaClassRes.ID = yogaClass.ID
	yogaClassRes.Name = yogaClass.Name
	yogaClassRes.Image = *GetYogaClassCoverImgRes(image)
	yogaClassRes.Description = yogaClass.Descriotion
	yogaClassRes.Type = yogaClass.Type
	yogaClassRes.Levels = *levelsRes
	yogaClassRes.UpdatedAt = yogaClass.UpdatedAt
	yogaClassRes.CreatedAt = yogaClass.CreatedAt
	c.JSON(http.StatusOK, yogaClassRes)
}

func (ygch *YogaClassHandler) GetCustomYogaClassByUserId(c *gin.Context) {
	user, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": "unauthorized",
		})
		return
	}
	classes, err := ygch.YogaClassService.GetCustomClassByUserId(user.ID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}

	res := []models.CustomClassRes{}
	for _, class := range *classes {
		temp := new(models.CustomClassRes)
		image, err := ygch.YogaClassCoverImgService.GetYogaClassCoverImgById(class.CoverImageID)
		if err == nil {
			temp.Image = *GetYogaClassCoverImgRes(image)
		} else if err.Error() == "record not found" {
			temp.Image.Filename = ""
			temp.Image.Url = ""
		} else if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		pose, err := ygch.DefinedPoseService.GetDefinedPoseResByClassId(class.ID)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		temp.ID = class.ID
		temp.Name = class.Name
		temp.Description = class.Descriotion
		temp.Type = class.Type
		temp.UserID = class.UserID
		temp.Poses = *pose
		temp.UpdatedAt = class.UpdatedAt
		temp.CreatedAt = class.CreatedAt
		res = append(res, *temp)
	}
	c.JSON(http.StatusOK, res)
}

func (ygch *YogaClassHandler) GetCustomYogaClassById(c *gin.Context) {
	res := models.CustomClassRes{}
	idStr := c.Param("id")
	idInt, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	class, err := ygch.YogaClassService.GetCustomClassById(uint(idInt))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	image, err := ygch.YogaClassCoverImgService.GetYogaClassCoverImgById(class.CoverImageID)
	if err == nil {
		res.Image = *GetYogaClassCoverImgRes(image)
	} else if err.Error() == "record not found" {
		res.Image.Filename = ""
		res.Image.Url = ""
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	pose, err := ygch.DefinedPoseService.GetDefinedPoseResByClassId(class.ID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	res.ID = class.ID
	res.Name = class.Name
	res.Description = class.Descriotion
	res.Type = class.Type
	res.UserID = class.UserID
	res.Poses = *pose
	res.UpdatedAt = class.UpdatedAt
	res.CreatedAt = class.CreatedAt
	c.JSON(http.StatusOK, res)
}
