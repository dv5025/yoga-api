package handlers

import (
	"net/http"
	"yoga-api/models"

	"github.com/gin-gonic/gin"
)

type HistpryHandler struct {
	historyService     models.HistoryService
	classService       models.YogaClassService
	levelService       models.YogaClassLevelService
	definedPoseService models.DefinedPoseService
}

func NewHistpryHandler(historyService models.HistoryService, classService models.YogaClassService, levelService models.YogaClassLevelService, definedPoseService models.DefinedPoseService) *HistpryHandler {
	return &HistpryHandler{historyService, classService, levelService, definedPoseService}
}

func (hh *HistpryHandler) AddHistory(c *gin.Context) {
	user, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": "unauthorized",
		})
		return
	}
	addHistoryReq := new(models.AddHistoryReq)
	if err := c.BindJSON(addHistoryReq); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	yogaClass, err := hh.classService.GetYogaClassById(addHistoryReq.ClassID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	if yogaClass.Type == "CUSTOM" && addHistoryReq.LevelID != 0 {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "custom class level id must be 0",
		})
		return
	}
	if yogaClass.Type == "PRESET" && addHistoryReq.LevelID <= 0 {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "preset class level id must be more than 0",
		})
		return
	}
	if addHistoryReq.LevelID > 0 {
		level, err := hh.levelService.GetYogaClassLevelById(addHistoryReq.LevelID)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		if level.YogaClassID != yogaClass.ID {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": "level not match with yoga class",
			})
			return
		}
	}
	if err := hh.historyService.AddHistory(user.ID, addHistoryReq.ClassID, addHistoryReq.LevelID); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, gin.H{
		"message": "history added",
	})
}

func (hh *HistpryHandler) GetUserHistories(c *gin.Context) {
	user, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": "unauthorized",
		})
		return
	}
	histories, err := hh.historyService.GetHistoriesByUserId(user.ID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := []models.HistoryRes{}
	for _, history := range *histories {
		temp := new(models.HistoryRes)
		yogaClass, err := hh.classService.GetYogaClassShortResById(history.YogaClassID)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		level, err := hh.levelService.GetYogaClassLevelById(history.YogaClassLevelID)
		if err == nil {
			pose, err := hh.definedPoseService.GetDefinedPoseResByClassIdAndLevelId(history.YogaClassID, level.ID)
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"message": err.Error(),
				})
				return
			}
			temp.LevelName = level.LevelName
			temp.Poses = *pose
		} else if err.Error() == "record not found" {
			pose, err := hh.definedPoseService.GetDefinedPoseResByClassId(history.YogaClassID)
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"message": err.Error(),
				})
				return
			}
			temp.LevelName = ""
			temp.Poses = *pose
		} else if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		temp.ID = history.ID
		temp.UserID = user.ID
		temp.YogaClass = *yogaClass
		temp.CreateAt = history.CreatedAt
		res = append(res, *temp)
	}
	c.JSON(http.StatusOK, res)
}
