package handlers

import "yoga-api/models"

func GetPostImgRes(image *models.PostImgTable) *models.PostImgRes {
	res := new(models.PostImgRes)
	res.Filename = image.Filename
	res.Url = image.Url
	return res
}
