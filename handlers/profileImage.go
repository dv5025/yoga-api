package handlers

import (
	"net/http"
	"yoga-api/models"

	"github.com/gin-gonic/gin"
)

type ProfileImageHandler struct {
	proImgS models.ProfileImageService
}

func NewProfileImageHandler(proImgS models.ProfileImageService) *ProfileImageHandler {
	return &ProfileImageHandler{proImgS}
}

func (proImgH *ProfileImageHandler) EditProfileImage(c *gin.Context) {
	user, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": "unauthorized",
		})
		return
	}

	form, err := c.MultipartForm()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}

	profileImage, err := proImgH.proImgS.EditProfileImage(form.File["image"][0], user.ID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"id":           user.ID,
		"email":        user.Email,
		"firstname":    user.Firstname,
		"lastname":     user.Lastname,
		"profileImage": profileImage.Url,
	})
}

func GetProfileImgRes(image *models.ProfileImageTable) *models.ProfileImageRes {
	res := new(models.ProfileImageRes)
	res.Filename = image.Filename
	res.Url = image.Url
	return res
}
