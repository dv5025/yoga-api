package hash

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"hash"
)

type HMAC struct {
	hmac hash.Hash
}

func NewHMAC(key string) *HMAC {
	return &HMAC{
		hmac: hmac.New(sha256.New, []byte(key)),
	}
}

func (h *HMAC) Hash(input string) (string, error) {
	h.hmac.Reset()
	if _, err := h.hmac.Write([]byte(input)); err != nil {
		return "", errors.New("cannot create token")
	}
	b := h.hmac.Sum(nil)
	return base64.URLEncoding.EncodeToString(b), nil
}
