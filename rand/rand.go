package rand

import (
	"crypto/rand"
	"encoding/base64"
)

const cost = 32

// random byte example
// GetToken return random byte
func GetToken() (string, error) {
	b := make([]byte, cost)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	return base64.URLEncoding.EncodeToString(b), nil
}
